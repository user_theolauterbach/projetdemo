package projet.demo.test.driver;

public class ConstantesTI {
	
	public static final String CHARACTERS = "http://gateway.marvel.com/v1/public/characters?ts=1&apikey=523d450e3a332ae9ec2f35afc322cdea&hash=a4a3a6f1af8e7ffcf89507c782acbd37";
	public static final String COMICS = "http://gateway.marvel.com/v1/public/comics?ts=1&apikey=523d450e3a332ae9ec2f35afc322cdea&hash=a4a3a6f1af8e7ffcf89507c782acbd37";
	public static final String CREATORS= "http://gateway.marvel.com/v1/public/creators?ts=1&apikey=523d450e3a332ae9ec2f35afc322cdea&hash=a4a3a6f1af8e7ffcf89507c782acbd37";
	public static final String EVENTS= "http://gateway.marvel.com/v1/public/events?ts=1&apikey=523d450e3a332ae9ec2f35afc322cdea&hash=a4a3a6f1af8e7ffcf89507c782acbd37";
	public static final String SERIES= "http://gateway.marvel.com/v1/public/series?ts=1&apikey=523d450e3a332ae9ec2f35afc322cdea&hash=a4a3a6f1af8e7ffcf89507c782acbd37";
	public static final String STORIES = "http://gateway.marvel.com/v1/public/stories?ts=1&apikey=523d450e3a332ae9ec2f35afc322cdea&hash=a4a3a6f1af8e7ffcf89507c782acbd37";
	
}