package projet.demo.test.driver;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

import static net.serenitybdd.rest.SerenityRest.given;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.test.context.ContextConfiguration;

import static io.restassured.config.DecoderConfig.decoderConfig;

@ContextConfiguration(locations = "/configure-les-tests.xml")
public class Configuration {

//	private final String pvkey = "3be959a0474d35db144b6e776e49a8673b78dc65";
//	private final String pukey = "523d450e3a332ae9ec2f35afc322cdea";
//	private final String ts = "1";
//	private final String hash = DigestUtils.md5Hex(ts+pukey+pvkey);
	
	private final String m_url;

	public Configuration
	(
			final String p_url
			)
	{
		super();
		this.m_url = p_url;
		
		// Pour éviter les problèmes d'accent dans les messages de retour du WS
		RestAssured.config = RestAssured.config().decoderConfig(decoderConfig().defaultContentCharset("UTF-8"));
	}

	/**
	 * Prépare la requête REST-Assured. Positione l'uri (endpoint+context) et les entêtes nécessaires.
	 * @return
	 */
	public RequestSpecification preparerDriver()
	{
		return given().baseUri(m_url);
	}
}
