package projet.demo.test.driver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import io.restassured.response.Response;
import projet.demo.test.steps.preparation.ProjetDemoPreparationSteps;

@ContextConfiguration(locations = "/configure-les-tests.xml")
public class AccesApi {

	private final static Logger LOGGER = LoggerFactory.getLogger(AccesApi.class);
	
	@Autowired
	private Configuration m_configuration;

	private ProjetDemoPreparationSteps m_preparateur;

	public String testAppelCharachters()
	{
		final Response reponse = m_configuration.preparerDriver().contentType("application/json")
				.get(ConstantesTI.CHARACTERS)//
				.then().extract().response();
		
		LOGGER.debug("*** Json reçu =" + reponse.body().asString());
		return null;
	}
	
	public String testAppelComics()
	{
		final Response reponse = m_configuration.preparerDriver().contentType("application/json")
				.get(ConstantesTI.COMICS)//
				.then().extract().response();
		
		LOGGER.debug("*** Json reçu =" + reponse.body().asString());
		return null;
	}
	
	public String testAppelCreators()
	{
		final Response reponse = m_configuration.preparerDriver().contentType("application/json")
				.get(ConstantesTI.CREATORS)//
				.then().extract().response();
		
		LOGGER.debug("*** Json reçu =" + reponse.body().asString());
		return null;
	}
	
	public String testAppelEvents()
	{
		final Response reponse = m_configuration.preparerDriver().contentType("application/json")
				.get(ConstantesTI.EVENTS)//
				.then().extract().response();
		
		LOGGER.debug("*** Json reçu =" + reponse.body().asString());
		return null;
	}
	
	public String testAppelSeries()
	{
		final Response reponse = m_configuration.preparerDriver().contentType("application/json")
				.get(ConstantesTI.SERIES)//
				.then().extract().response();
		
		LOGGER.debug("*** Json reçu =" + reponse.body().asString());
		return null;
	}
	
	public String testAppelStories()
	{
		final Response reponse = m_configuration.preparerDriver().contentType("application/json")
				.get(ConstantesTI.STORIES)//
				.then().extract().response();
		
		LOGGER.debug("*** Json reçu =" + reponse.body().asString());
		return null;
	}
}
