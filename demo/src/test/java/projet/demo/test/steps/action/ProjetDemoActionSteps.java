package projet.demo.test.steps.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import projet.demo.test.driver.AccesApi;
import projet.demo.test.steps.preparation.ProjetDemoPreparationSteps;

@ContextConfiguration(locations = "/configure-les-tests.xml")
public class ProjetDemoActionSteps 
{
	@Autowired
	AccesApi accesApi;

	@Step("j'apelle l'api avec le chemin Charachters")
	public ProjetDemoActionSteps japelleLApiCharachters()
	{
		accesApi.testAppelCharachters();
		return this;
	}
	
	@Step("j'apelle l'api avec le chemin Comics")
	public ProjetDemoActionSteps japelleLApiComics()
	{
		accesApi.testAppelComics();
		return this;
	}
	
	@Step("j'apelle l'api avec le chemin Creators")
	public ProjetDemoActionSteps japelleLApiCreators()
	{
		accesApi.testAppelCreators();
		return this;
	}
	
	@Step("j'apelle l'api avec le chemin Events")
	public ProjetDemoActionSteps japelleLApiEvents()
	{
		accesApi.testAppelEvents();
		return this;
	}
	
	@Step("j'apelle l'api avec le chemin Series")
	public ProjetDemoActionSteps japelleLApiSeries()
	{
		accesApi.testAppelSeries();
		return this;
	}
	
	@Step("j'apelle l'api avec le chemin Stories")
	public ProjetDemoActionSteps japelleLApiStories()
	{
		accesApi.testAppelStories();
		return this;
	}
}