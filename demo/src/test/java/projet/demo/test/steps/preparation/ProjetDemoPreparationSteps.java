package projet.demo.test.steps.preparation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import projet.demo.test.steps.action.ProjetDemoActionSteps;
import projet.demo.test.steps.verification.ProjetDemoVerificationSteps;

@ContextConfiguration(locations = "/configure-les-tests.xml")
public class ProjetDemoPreparationSteps 
{
	@Autowired
	
	@Steps(shared = true)
	protected ProjetDemoPreparationSteps m_prepateurLego;

	@Step("une URL")
	public ProjetDemoPreparationSteps uneURL()
	{
		String url = "developer.marvel.com";
		return this;
	}
}