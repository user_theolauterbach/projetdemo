package projet.demo.test.steps.verification;

import static org.assertj.core.api.Assertions.assertThat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import projet.demo.test.steps.action.ProjetDemoActionSteps;
import projet.demo.test.steps.preparation.ProjetDemoPreparationSteps;

import static net.serenitybdd.rest.SerenityRest.then;

@ContextConfiguration(locations = "/configure-les-tests.xml")
public class ProjetDemoVerificationSteps
{
	@Autowired

	@Steps(shared = true)
	protected ProjetDemoPreparationSteps m_prepateurLego;

	@Step("une verification")
	public ProjetDemoVerificationSteps leResultatEstOk()
	{
		/**
		 * Assert sur le code statut HTTP de la réponse
		 */
		assertThat(then().extract().response().getStatusCode()).isEqualTo(200);
		return this;
	}
}