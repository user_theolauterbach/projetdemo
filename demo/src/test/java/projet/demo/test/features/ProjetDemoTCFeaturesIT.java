package projet.demo.test.features;

import org.junit.Test;
import org.junit.runner.RunWith;

import net.serenitybdd.junit.runners.SerenityRunner;

@RunWith(SerenityRunner.class)
public class ProjetDemoTCFeaturesIT extends ProjetDemoFeaturesIT
{

	@Test
	public void test1()
	{
		m_etantDonne.uneURL();
		m_lorsque.japelleLApiCharachters();
		m_alors.leResultatEstOk();
	}
	
	@Test
	public void test2()
	{
		m_etantDonne.uneURL();
		m_lorsque.japelleLApiComics();
		m_alors.leResultatEstOk();
	}
	
	@Test
	public void test3()
	{
		m_etantDonne.uneURL();
		m_lorsque.japelleLApiCreators();
		m_alors.leResultatEstOk();
	}
	
	@Test
	public void test4()
	{
		m_etantDonne.uneURL();
		m_lorsque.japelleLApiEvents();
		m_alors.leResultatEstOk();
	}
	
	@Test
	public void test5()
	{
		m_etantDonne.uneURL();
		m_lorsque.japelleLApiSeries();
		m_alors.leResultatEstOk();
	}
	
	@Test
	public void test6()
	{
		m_etantDonne.uneURL();
		m_lorsque.japelleLApiStories();
		m_alors.leResultatEstOk();
	}
}