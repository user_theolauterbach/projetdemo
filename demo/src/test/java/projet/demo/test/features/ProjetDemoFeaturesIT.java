package projet.demo.test.features;

import net.thucydides.core.annotations.Steps;
import projet.demo.test.steps.action.ProjetDemoActionSteps;
import projet.demo.test.steps.preparation.ProjetDemoPreparationSteps;
import projet.demo.test.steps.verification.ProjetDemoVerificationSteps;

public abstract class ProjetDemoFeaturesIT
{
   @Steps(shared = true)
   ProjetDemoPreparationSteps m_etantDonne;

   @Steps(shared = true)
   ProjetDemoActionSteps m_lorsque;

   @Steps(shared = true)
   ProjetDemoVerificationSteps m_alors;

}